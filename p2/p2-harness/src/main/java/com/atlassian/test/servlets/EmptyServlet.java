package com.atlassian.test.servlets;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Scanned
public class EmptyServlet extends HttpServlet {
    private static final String WRM_REQUIRE_RESOURCE_KEY = "com.atlassian.plugins.atlassian-plugins-webresource-plugin:web-resource-manager";
    private final WebResourceManager webResourceManager;

    public EmptyServlet(
        WebResourceManager webResourceManager
    ) {
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        webResourceManager.requireResource(WRM_REQUIRE_RESOURCE_KEY);
        resp.getWriter().write("<!doctype html>\n" +
            "<html>\n" +
            "<head>\n" +
            "    <title>An empty page</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "    <p>Hello, world!</p>\n" +
            "    <p>There should be a <code>WRM.require</code> function on the page now.</p>\n" +
            "    <p>Try running something like:</p>\n" +
            "    <pre><code>WRM.require(['com.atlassian.auiplugin:ajs'], function() {\n" +
            "        console.log('loaded', {AJS});\n" +
            "    })</code></pre>\n");
        resp.getWriter().write(webResourceManager.getRequiredResources(UrlMode.AUTO));
        resp.getWriter().write("</body></html>");
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().close();
    }
}
