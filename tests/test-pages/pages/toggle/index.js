require(['require', 'jquery', 'aui-test/toggle-server'], function(require) {
    const $ = require('jquery');
    const { url, server } = require('aui-test/toggle-server');
    let ourUrl = url;

    $(document).on('change', '#toggle-controller-delay', function (e) {
        server.autoRespondAfter = e.target.value;
    });

    $(document).on('change', '#toggle-controller-success', function (e) {
        ourUrl = (e.target.checked) ? url :  'wrongUrl';
    });

    $(document).on('change', 'div.triggers aui-toggle', function (e) {
        const toggle = e.currentTarget;
        const isChecked = toggle.checked;
        toggle.busy = true;

        $.post(ourUrl, {value: isChecked})
            .done(function() {
                console.log('success');
            })
            .fail(function() {
                toggle.checked = !isChecked;
                console.error('display an error message ');
            }).always(function () {
                toggle.busy = false;
            });
    });

    $(document).on('submit', '#my-form', function(e) {
        const $result = $('#my-form-result');
        const $form = $(e.target);
        e.preventDefault();
        $result.val($form.serialize());
    });

});
