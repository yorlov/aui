import $ from '@atlassian/aui/src/js/aui/jquery';
import deduplicateIDs from '@atlassian/aui/src/js/aui/internal/deduplicateIDs'


function assertNotDuplicated($wrapper, selector) {
    expect($wrapper.find(selector).length, 'the id is not properly replaced').to.equal(0);
}

describe('deduplicateIDs', function () {
    it('can deduplicate ids', function () {
        let dom = $(`<div id="test">
                            <div id="nested">
                                <button aria-controls="more-details"></button>
                                <aui-inline-dialog id="more-details"></aui-inline-dialog>
                                <a href="#dwarfers" aria-owns="deprecated" aria-haspopup="true">deprecated trigger</a>
                                <div id="deprecated" class="aui-style-default aui-dropdown2"></div>
                            </div>
                        </div>`);

        function idGenerator() {
            let index = -1;
            const ids = ['a', 'b', 'c', 'd'];
            return function () {
                index++;
                return ids[index]
            }
        }

        deduplicateIDs(dom, idGenerator());

        const $wrapper = $('<div></div>');
        $wrapper.append(dom);

        assertNotDuplicated($wrapper, '#test');
        assertNotDuplicated($wrapper, '#nested');
        assertNotDuplicated($wrapper, '#more-details');
        assertNotDuplicated($wrapper, '[aria-controls="more-details"]');
        assertNotDuplicated($wrapper, '#deprecated');
        assertNotDuplicated($wrapper, '[aria-owns="deprecated"]');
    })
});
