# AUI

The AUI library is a set of patterns and components for building user interfaces in
Atlassian products and services.

## Documentation

Thorough documentation is available at [the AUI website](https://aui.atlassian.com).

* [Component documentation](https://aui.atlassian.com/latest/)
* [Changelog](https://bitbucket.org/atlassian/aui/src/master/CHANGELOG.md?at=master)

## Browser compatibility

- Chrome latest stable
- Firefox latest stable
- Safari latest stable (on OS X only)
- IE 11+

## How do you get it?

Consuming the AUI library is supported through a few methods:

### Install as a Node package

AUI is released to [npmjs.com](https://www.npmjs.com/package/@atlassian/aui).
Install it through your favourite package manager:

`npm install @atlassian/aui`
`yarn add @atlassian/aui`

In the Node package, you will find:

  * `dist/` contains pre-compiled javascript and css. This is the simplest way to use AUI.

  * `src/` contains the raw JavaScript and LESS sources. It's unlikely you'll require these directly.

### Install as an Atlassian plugin

AUI can be used as [an Atlassian P2 plugin][atl-P2].
This plugin requires the following technologies to be available in the runtime it is installed in to:

* [Web Resource Manager (aka the WRM)][atl-WRM] version 3.6.0 or higher
* [Spring Scanner][atl-SS] version 2 or higher

All Atlassian Server products come with AUI pre-installed, so you don't need to do much to re-use it in your plugin.

Each AUI component has a `web-resource` key you can include it by. Consult each component's documentation
on [the AUI website](https://aui.atlassian.com/latest/) for the key.

### Download a distribution

AUI distributions are released as a zip file called the
[aui-flat-pack](https://packages.atlassian.com/maven-public/com/atlassian/aui/aui-flat-pack/), hosted
through Atlassian's Maven nexus.
Note that this is equivalent to the `dist/` folder available in the Node package.

### Consume through a CDN

Use of AUI is not officially supported through a Content Delivery Network (CDN).
However, because AUI is published to npmjs.com, the AUI distributions are also published through
some public CDN services such as:

* [cdnjs.com](https://cdnjs.com/libraries/aui)
* [unpkg.com](https://unpkg.com/@atlassian/aui@latest/)


## Raising issues

Raise bugs or feature requests in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

## Contributing to AUI

Refer [Contribution guidelines](CONTRIBUTING.md)

## License

This is a [mono-repo](https://github.com/babel/babel/blob/master/doc/design/monorepo.md),
which means that different parts of this repository can have different licenses.

The base level of the repository is licensed under the [Apache 2.0 license][license_apache2.0].

For each package in the repository, there are separate license files (`LICENSE.md`) that
specify the license restrictions for their code.

Please note that some packages are licensed under either
the [Atlassian Design Guidelines (ADG) license][license_adg] or
the [Atlassian Developer Terms license][license_adt], which come with their
own terms and conditions.

If you fork this repository, you can continue to use the Atlassian-licensed packages
only under the given license restrictions. If you want to redistribute this repository,
you will need to replace the Atlassian-licensed components with your own implementations.

Copyright (c) 2018 Atlassian and others.


[atl-P2]: https://developer.atlassian.com/server/framework/atlassian-sdk/plugin-framework/
[atl-SS]: https://bitbucket.org/atlassian/atlassian-spring-scanner
[atl-WRM]: https://bitbucket.org/atlassian/atlassian-plugins-webresource
[license_apache2.0]: https://www.apache.org/licenses/LICENSE-2.0
[license_adg]: https://atlassian.design/guidelines/handy/license
[license_adt]: https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/
