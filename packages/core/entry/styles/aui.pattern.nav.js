import './aui.page.reset';
import './aui.page.typography';
import './aui.page.links';
// todo: handle dep chain for interop styles of dropdown2 + horizontal nav
import '@atlassian/aui/src/less/aui-navigation.less';
