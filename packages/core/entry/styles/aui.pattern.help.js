// todo: split help in to its individual patterns so that this file can be made smaller.
import './aui.page.reset';
import './aui.page.typography';
import './aui.page.links';
import './aui.pattern.page-layout';
import './aui.pattern.inline-dialog';
import '@atlassian/aui/src/less/adg-help.less';
