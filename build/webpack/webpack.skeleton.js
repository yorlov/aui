const merge = require('webpack-merge');
const parts = require('./webpack.parts');
const libraryExternals = require('./webpack.externals');

const librarySkeleton = merge([
    parts.setAuiVersion(),

    parts.transpileJs({
        exclude: /(node_modules|bower_components|js-vendor|compiled-soy)/,
        options: {
            cacheDirectory: true,
        },
    }),

    parts.loadSoy(),
    parts.resolveSoyDeps(),

    parts.loadFonts({
        options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
        }
    }),

    parts.loadImages({
        options: {
            name: '[name].[ext]',
            outputPath: 'images/',
            limit: 3 * 1024,
        },
    }),

    parts.extractCss(),

    parts.production()
]);

module.exports = {
    librarySkeleton,
    libraryExternals,
};
