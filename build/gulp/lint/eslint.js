'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var eslint = require('gulp-eslint');

module.exports = function lintWithEslint () {
    var opts = Object.assign({
        files: [
            'gulpfile.js',
            'packages/core/src/**/*.js',
            'packages/docs/src/**/*.js',
            'tests/**/*.js'
        ]
    }, gat.opts());
    return gulp.src(opts.files)
        .pipe(eslint())
        .pipe(eslint.format());
};
